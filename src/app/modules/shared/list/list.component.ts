import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Movie} from "../../../model/Movie";
import {MovieService} from "../../../services/movie.service";
import {Observable, Subscription} from "rxjs";
import {SelectItem} from "primeng/api";
import {ActivatedRoute, Router} from "@angular/router";
import {MovieHomeComponent} from "../../pages/movie-home/movie-home.component";
import {Item} from "../../../model/Item";
import {TvShowHomeComponent} from "../../pages/tv-show-home/tv-show-home.component";
import {MovieAddComponent} from "../../feature/movies/movie-add/movie-add.component";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit { //Child
  @Input()
  myItemList: Item[] = [];
  @Output()
  selected: EventEmitter<Item> = new EventEmitter<Item>();

  sortKey: string = "";

  sortOptions: SelectItem[] = [];

  sortOrder: number = 0;

  sortField: string = '';
  hideAddButton: boolean;

  constructor(private activateRoute: ActivatedRoute, private router: Router) {
    let currentParentComponent = activateRoute.snapshot.component;
    this.hideAddButton = currentParentComponent !== MovieHomeComponent
      && currentParentComponent !== TvShowHomeComponent;
  }

  ngOnInit(): void {
    this.sortOptions = [
      {label: 'Title A>Z', value: 'title'},
      {label: 'Title Z>A', value: '!title'}
    ];
  }

  onSortChange(event: any) {
    let value = event.value;

    if (value.indexOf('!') === 0) {
      this.sortOrder = -1;
      this.sortField = value.substring(1, value.length);
    } else {
      this.sortOrder = 1;
      this.sortField = value;
    }
  }


}
