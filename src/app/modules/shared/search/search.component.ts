import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {NgForm} from "@angular/forms";
import {Item} from "../../../model/Item";
import {Subject, Subscription} from "rxjs";
import {debounceTime, distinctUntilChanged, filter} from "rxjs/operators";

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit , OnDestroy{

  inputTitle$ = new Subject<string>();
  @Output() searchedItem: EventEmitter<string> = new EventEmitter<string>();
  private subscription: Subscription | undefined;

  constructor() {
  }

  ngOnInit(): void {
    this.subscription = this.inputTitle$.pipe(
      debounceTime(1000),
      distinctUntilChanged(),
      filter(value => {
        return value.length > 2 || value.length === 0
      })
    ).subscribe(value => this.searchedItem.emit(value));
  }

  ngOnDestroy(): void {
    this.subscription?.unsubscribe();
  }


}
