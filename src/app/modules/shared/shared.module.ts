import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ListComponent} from "./list/list.component";
import {SearchComponent} from "./search/search.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DataViewModule} from "primeng/dataview";
import {DropdownModule} from "primeng/dropdown";
import {ButtonModule} from "primeng/button";
import {CardModule} from "primeng/card";
import {InputTextModule} from "primeng/inputtext";
import {InputSwitchModule} from "primeng/inputswitch";
import {NumericControlComponent} from './numeric-control/numeric-control.component';
import {RippleModule} from "primeng/ripple";
import {ControlErrorComponent} from './control-error/control-error.component';
import {FormSubmitDirective} from './directives/form-submit.directive';
import {ControlErrorsDirective} from "./directives/control-error.directive";
import {InputTextareaModule} from "primeng/inputtextarea";
import {RadioButtonModule} from "primeng/radiobutton";
import {CoreModule} from "../core/core.module";


@NgModule({
  declarations: [ListComponent, SearchComponent, ControlErrorsDirective, NumericControlComponent, ControlErrorComponent, FormSubmitDirective],
  imports: [
    CommonModule,
    FormsModule,
    DataViewModule,
    DropdownModule,
    ButtonModule,
    CardModule,
    InputTextModule,
    InputSwitchModule,
    RippleModule
  ],
  exports: [CommonModule, ListComponent, SearchComponent, ReactiveFormsModule,
    FormsModule,
    ButtonModule,
    CardModule,
    InputTextModule,
    InputSwitchModule,
    NumericControlComponent,
    ControlErrorsDirective,
    FormSubmitDirective,
    InputTextareaModule,
    ControlErrorComponent,
    RadioButtonModule],
  entryComponents: [ControlErrorComponent],

})
export class SharedModule {
}
