import {NgModule} from '@angular/core';
import {MovieAddComponent} from "./movie-add/movie-add.component";
import {MovieDetailComponent} from "./movie-detail/movie-detail.component";
import {SharedModule} from "../../shared/shared.module";
import {MovieService} from "../../../services/movie.service";
import {MoviesRoutingModule} from "./movies-routing.module";
import {MovieResolver} from "../../../services/movie.resolver";


@NgModule({
  declarations: [MovieAddComponent, MovieDetailComponent],
  imports: [
    SharedModule, MoviesRoutingModule
  ],
  providers: [MovieService, MovieResolver]
})
export class MoviesModule {
}
