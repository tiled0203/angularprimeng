import {Component, OnInit} from '@angular/core';
import {NgForm, NgModel} from "@angular/forms";
import {MovieService} from "../../../../services/movie.service";
import {Movie} from "../../../../model/Movie";
import {Router} from "@angular/router";
import {Item} from "../../../../model/Item";
import {Observable} from "rxjs";

@Component({
  selector: 'app-movie-add',
  templateUrl: './movie-add.component.html',
  styleUrls: ['./movie-add.component.css']
})
export class MovieAddComponent implements OnInit {
  foundMovies: Movie[] = [];
  online: boolean = false;
  searchedTitle: string = "";

  constructor(private movieService: MovieService, private router: Router) {
  }

  ngOnInit(): void {
  }

  addSelectedMovieAndDetail(movie: Movie) {
    this.movieService.addMovie(movie.onlineId).then(movieResponse => {
      this.router.navigate(["/movie/detail", movieResponse.id]);
    });
  }

  search(searchedTitle: string) {
    // console.log(ngForm.form.controls.title.errors);
    // if (ngForm.valid) {
    this.movieService.lookupMovie(searchedTitle, this.online).subscribe(movieResponse => this.foundMovies = movieResponse);
    // }
    this.searchedTitle = searchedTitle;
  }
}
