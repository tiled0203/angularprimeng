import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {TvShowService} from "../../../../services/tv-show.service";
import {Router} from "@angular/router";
import {forbiddenTitleValidator} from "../../../../model/validators/ForbiddenTitleValidator";
import {takenTitleValidator} from "../../../../model/validators/TakenTitleValidator";

@Component({
  selector: 'app-tv-show-new',
  templateUrl: './tv-show-new.component.html',
  styleUrls: ['./tv-show-new.component.css']
})
export class TvShowNewComponent implements OnInit {
  newTvShowForm: FormGroup = new FormGroup({});
  customErrors = {titleTaken: 'Sorry this title is already been taken'}

  constructor(private fb: FormBuilder, private tvShowService: TvShowService, private router: Router) {
  }

// {
//   "title": "string",
//   "year": 0,
//   "runtime": 0,
//   "seasons": 0,
//   "episodes": 0,
//   "overview": "string",
//   "posterUrl": "string"
// }
  tvShowControls = new FormArray([]);

  ngOnInit(): void {
    this.newTvShowForm = this.fb.group({
        title: ['', [Validators.required, Validators.minLength(2), forbiddenTitleValidator(/test/i)],
          takenTitleValidator(this.tvShowService)
        ],
        toggleTvShowRadioRb: "radio",
        tvshow: this.tvShowControls,
        // year: [2000, [Validators.min(1990), Validators.max(new Date().getFullYear())]],
        // runtime: [0, Validators.min(0)],
        // seasons: [1, Validators.min(1)],
        // episodes: [1, Validators.min(1)],
        overview: ['', [Validators.required, Validators.minLength(20)]],
        posterUrl: ['', Validators.required]
      }
    );
  }

  addNewTvShow() {
    let body: any = {};
    for (const bodyElement in this.newTvShowForm.value) {
      if (bodyElement === 'tvshow') {
        for (const bodyElementElement in this.newTvShowForm.value[bodyElement][0]) {
          body[bodyElementElement] = this.newTvShowForm.value[bodyElement][0][bodyElementElement];
        }
      } else {
        body[bodyElement] = this.newTvShowForm.value[bodyElement];
      }
    }
    body['toggleTvShowRadioRb'] = undefined;
    this.tvShowService.addNewTvShow(body)
      .subscribe(tvShowResponse => this.router.navigate(["tvshow/detail", tvShowResponse.id]));
  }

  toggleTvShow(isTvShow: boolean) {
    console.log(isTvShow)
    if (isTvShow) {
      let controls = this.fb.group({
        year: [2000, [Validators.min(1990), Validators.max(new Date().getFullYear())]],
        runtime: [0, [Validators.min(0)]],
        seasons: [1, [Validators.min(1)]],
        episodes: [1, [Validators.min(1)]]
      });
      this.tvShowControls.push(controls);
    } else {
      this.tvShowControls.clear();
    }
  }
}
