import {NgModule} from '@angular/core';
import {TvShowAddComponent} from "./tv-show-add/tv-show-add.component";
import {TvShowDetailComponent} from "./tv-show-detail/tv-show-detail.component";
import {TvShowService} from "../../../services/tv-show.service";
import {SharedModule} from "../../shared/shared.module";
import {TvShowRoutingModule} from "./tv-show-routing.module";
import {TvShowNewComponent} from './tv-show-new/tv-show-new.component';


@NgModule({
  declarations: [TvShowAddComponent, TvShowDetailComponent, TvShowNewComponent],
  imports: [
    SharedModule, TvShowRoutingModule
  ],
  providers: [TvShowService]
})
export class TvShowModule { }
