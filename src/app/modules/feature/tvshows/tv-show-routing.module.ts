import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {TvShowAddComponent} from "./tv-show-add/tv-show-add.component";
import {TvShowDetailComponent} from "./tv-show-detail/tv-show-detail.component";
import {TvShowHomeComponent} from "../../pages/tv-show-home/tv-show-home.component";
import {TvShowNewComponent} from "./tv-show-new/tv-show-new.component";

const routes: Routes = [
  {path: 'add', component: TvShowAddComponent},
  {path: 'new', component: TvShowNewComponent},
  {path: 'detail/:id', component: TvShowDetailComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TvShowRoutingModule {
}
