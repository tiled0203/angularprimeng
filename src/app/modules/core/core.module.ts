import {APP_INITIALIZER, NgModule, Optional, SkipSelf} from '@angular/core';
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {AuthInterceptor} from "./auth.interceptor";
import {HeaderComponent} from "./header/header.component";
import {HomeComponent} from "./home/home.component";
import {ButtonModule} from "primeng/button";
import {DropdownModule} from "primeng/dropdown";
import {CardModule} from "primeng/card";
import {MenubarModule} from "primeng/menubar";
import {ConfigService} from "./config.service";
import {AuthService} from "./auth.service";
import {LoginComponent} from './login/login.component';
import {FormsModule} from "@angular/forms";
import {AuthGuard} from "./auth.guard";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MovieResolver} from "../../services/movie.resolver";
import {PreloadStrategy} from "./preload-strategy.service";


@NgModule({
  declarations: [HeaderComponent, HomeComponent, LoginComponent],
  imports: [
    FormsModule,
    ButtonModule,
    DropdownModule,
    CardModule,
    MenubarModule
  ],
  exports: [
    HeaderComponent, HomeComponent,
    FormsModule,
    ButtonModule,
    DropdownModule,
    CardModule,
    MenubarModule,
    BrowserAnimationsModule
  ],
  providers: [MovieResolver,AuthService, AuthGuard, PreloadStrategy,
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
    ConfigService,
    {
      provide: APP_INITIALIZER,
      useFactory: (config: ConfigService) => () => config.load(),
      deps: [ConfigService],
      multi: true
    }]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error("CoreModule is already loaded");
    }
  }
}
