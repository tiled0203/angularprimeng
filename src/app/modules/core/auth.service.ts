import {Injectable} from '@angular/core';

@Injectable()
export class AuthService {

  loggedIn: boolean = false;

  constructor() {
  }

  login(userName: string, password: string) {
    console.log(userName, password);
    this.loggedIn = userName === password;
  }

}
