import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {TvShow} from "../../../model/TvShow";
import {TvShowService} from "../../../services/tv-show.service";
import {Router} from "@angular/router";
import {Item} from "../../../model/Item";
import {Subject} from "rxjs";

@Component({
  selector: 'app-tv-show-home',
  templateUrl: './tv-show-home.component.html',
  styleUrls: ['./tv-show-home.component.css']
})
export class TvShowHomeComponent implements OnInit {

  tvShows: TvShow[] = [];
  online: boolean = false;
  searchedTitle = "";

  constructor(private tvShowService: TvShowService, private router: Router) {
  }


  ngOnInit(): void {
    this.fetchTvShows();
  }

  private fetchTvShows() {
    this.tvShowService.getTvShows().then(tvShowsResponse => this.tvShows = tvShowsResponse);
  }

  gotoDetail(item: Item) {
    this.router.navigate(['tvshow/detail', item.id]);
  }

  delete(item: Item) {
    this.tvShowService.delete(item.id).then(() => {
      this.fetchTvShows();
    });
  }

  search(searchedTitle: string) {
    this.tvShowService.lookupTvShow(searchedTitle, this.online).then(tvShowResponse => this.tvShows = tvShowResponse);
    this.searchedTitle = searchedTitle;
  }
}
