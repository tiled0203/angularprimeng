import {Component, OnDestroy, OnInit} from '@angular/core';
import {MovieService} from "../../../services/movie.service";
import {Movie} from "../../../model/Movie";
import {Router} from "@angular/router";
import {Subject, Subscription} from "rxjs";

@Component({
  selector: 'app-movie-home',
  templateUrl: './movie-home.component.html',
  styleUrls: ['./movie-home.component.css']
})
export class MovieHomeComponent implements OnInit, OnDestroy { //Parent
  foundMovies: Movie[] = [];
  private subscription: Subscription | undefined;

  online$ = new Subject<boolean>();
  online = false;
  title: string = "";

  constructor(private movieService: MovieService, private router: Router) {
  }

  ngOnInit(): void {
    this.movieService.getMovies().then(movieResponse => {
      this.foundMovies = movieResponse
    });

    this.movieService.test().subscribe(value => {
        console.log("subscribed! ", value);
      }, error => console.log(error),
      () => console.log("Done sending"));

    this.online$.subscribe(value => {
      this.online = value
      this.search(this.title);
    });
  }

  showDetail(movie: Movie) {
    this.router.navigate(["movie/detail", movie.id]);
  }

  ngOnDestroy(): void {
    console.log("component destroyed")
    // this.subscription?.unsubscribe();
  }

  search(title: string) {
    // console.log(ngForm.form.controls.title.errors);
    // if (ngForm.valid) {
    this.title = title;
    this.movieService.lookupMovie(this.title, this.online).subscribe(movieResponse => {
      this.foundMovies = movieResponse
    });

    // }
  }
}
