import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MovieHomeComponent} from "./movie-home/movie-home.component";
import {TvShowHomeComponent} from "./tv-show-home/tv-show-home.component";
import {SharedModule} from "../shared/shared.module";


@NgModule({
  declarations: [MovieHomeComponent, TvShowHomeComponent],
  imports: [
    SharedModule
  ]
})
export class PagesModule {
}
