import {Item} from "./Item";

export interface TvShow extends Item{
  status: string;
  overview: string;
}


class TvShowObj implements TvShow{
  id: number;
  onlineId: string;
  overview: string;
  poster: string;
  rating: number;
  runtime: number;
  status: string;
  title: string;
  year: number;


  constructor(id: number, onlineId: string, overview: string, poster: string, rating: number, runtime: number, status: string, title: string, year: number) {
    this.id = id;
    this.onlineId = onlineId;
    this.overview = overview;
    this.poster = poster;
    this.rating = rating;
    this.runtime = runtime;
    this.status = status;
    this.title = title;
    this.year = year;
  }
}
