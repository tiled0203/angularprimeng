import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {MovieHomeComponent} from "./modules/pages/movie-home/movie-home.component";
import {TvShowHomeComponent} from "./modules/pages/tv-show-home/tv-show-home.component";
import {HomeComponent} from "./modules/core/home/home.component";
import {LoginComponent} from "./modules/core/login/login.component";
import {PreloadStrategy} from "./modules/core/preload-strategy.service";

export const routes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'tvshow/home', component: TvShowHomeComponent},
  {
    path: 'tvshow',
    loadChildren: () => import('./modules/feature/tvshows/tv-show.module').then(value => value.TvShowModule),
    data: {preload: true}
  },
  // {path: 'tvshow/add', component: TvShowAddComponent, canActivate: [AuthGuard]},
  // {path: 'tvshow/detail/:id', component: TvShowDetailComponent},
  {path: 'movie/home', component: MovieHomeComponent},
  {
    path: 'movie',
    loadChildren: () => import('./modules/feature/movies/movies.module').then(value => value.MoviesModule)
  },
  // {path: 'movie/add', component: MovieAddComponent, canActivate: [AuthGuard]},
  // {path: 'movie/detail/:id', component: MovieDetailComponent},
  {path: '**', redirectTo: 'home'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {preloadingStrategy: PreloadStrategy})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
