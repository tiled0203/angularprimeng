import {Injectable} from '@angular/core';
import {Movie, MovieAdapter} from "../model/Movie";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Observable, Subject} from "rxjs";
import {map, switchMap, timeInterval} from "rxjs/operators";
import {ConfigService} from "../modules/core/config.service";

@Injectable({
  providedIn: 'root'
})
export class MovieService {
  private static count: number = 0;

  constructor(private http: HttpClient, private config: ConfigService, private movieAdapter: MovieAdapter) {
    console.log("env: ", config.getEnv("env"))
    console.log("host:", config.getConfig("host"))
  }

  getMovies() {
    // let observable = new Observable<any[]>(subscriber => {
    //     subscriber.next([{
    //       id: 1, onlineId: "test", plot: "test", rating: 5, runtime: 50, seen: true, movietitle: "LOTR", year: 2000
    //     }])
    //     subscriber.complete();
    //   }
    // );
    return this.http.get<any[]>(environment.movieApiUrl).pipe(map((movies: Movie[]) => movies.map(value => this.movieAdapter.adapt(value)))).toPromise();
  }

  // id: number;
  // onlineId: string;
  // plot: string;
  // poster: string;
  // rating: number;
  // runtime: number;
  // seen: boolean;
  // title: string;
  // year: number;
  findMovie(id: number) {
    return this.http.get<Movie>(`${environment.movieApiUrl}/${id}`).pipe(map(value => this.movieAdapter.adapt(value)));
  }

  lookupMovie(searchTitle: string, online: boolean) {
    return this.http.get<any[]>(`${environment.movieApiUrl}/search`, {
      params: {
        title: searchTitle,
        online: online
      }
    }).pipe(map((movies: Movie[]) => movies.map(value => this.movieAdapter.adapt(value))));
  }

  addMovie(onlineId: string | undefined) {
    return this.http.post<Movie>(`${environment.movieApiUrl}`, {"apiId": onlineId}).toPromise();
  }

  delete(id: number | undefined) {
    return this.http.delete(`${environment.movieApiUrl}/${id}`).toPromise();
  }

  test() {
    MovieService.count += 1;
    return new Observable(subscriber => {
      setInterval(() => {
        subscriber.next("subscribed count= " + MovieService.count + " Today=  " + new Date());
      }, 2000)
      setTimeout(() => {
        subscriber.error("Error !!! ");
      }, 4000)
      setTimeout(() => {
        subscriber.complete();
      }, 6000)
    });
  }

  // switchmap() {
  //   return this.http.get("/customer").pipe(
  //     switchMap(customer => this.http.get(`/address/${customer.id}`))
  //   ).subscribe(value => console.log(value));
  //
  //
  // }
}
