import {Injectable} from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import {Observable, of} from 'rxjs';
import {MovieService} from "./movie.service";
import {Movie} from "../model/Movie";

@Injectable()
export class MovieResolver implements Resolve<Movie> {

  constructor(private movieService: MovieService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Movie> {
    console.log(route.paramMap);
    return this.movieService.findMovie(Number(route.paramMap.get('id')));
  }
}
